package flights.server.services;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import flights.common.Error;
import flights.common.Route;

@Path("/routes")
public class RouteService {
	
	private static Map<Integer, Route> routes = new HashMap<Integer, Route>();
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes({MediaType.APPLICATION_JSON })
	public Response addRoute(Route route) {
		
		if (routes.containsKey(route.getId()))
			return Response
					.status(Response.Status.CONFLICT)
					.entity(new Error(1, "A route with this id already exists"))
					.build();
		
		routes.put(route.getId(), route);
			
		return Response.ok().build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRoutes() {
		return Response.ok().entity(routes.values()).build();
	}
	
	@DELETE
	@Path("/{routeId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteRoutes(@PathParam("routeId") int routeId) {
		
		Route deletedRoute = routes.remove(routeId);
		
		if (deletedRoute == null)
			return Response
					.status(Response.Status.NOT_FOUND)
					.entity(new Error(2, "No route found with this id"))
					.build();
		
		return Response.ok().build();
	}
}
