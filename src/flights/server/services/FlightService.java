package flights.server.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/flights")
public class FlightService {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String searchFlights(@QueryParam("origin") String origin,
								@QueryParam("destination") String dest,
								@QueryParam("date") String date) {
		return String.format("flights : %s %s %s", origin, dest, date);
	}
	
	@PUT
	@Path("/{routeId}/{date}/seats")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes({MediaType.APPLICATION_JSON })
	public String addSeats(@PathParam("routeId") int routeId,
						   @PathParam("date") String date) {
		return String.format("addSeats : %d %s", routeId, date);
	}
	
	@POST
	@Path("/{routeId}/{date}/check-in")
	@Produces(MediaType.APPLICATION_JSON)
	public String beginCheckIn(@PathParam("routeId") int routeId,
						   	   @PathParam("date") String date) {
		return String.format("beginCheckIn : %d %s", routeId, date);
	}
	
	@DELETE
	@Path("/{routeId}/{date}/check-in")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes({MediaType.APPLICATION_JSON })
	public String endCheckIn(@PathParam("routeId") int routeId,
						     @PathParam("date") String date) {
		return String.format("endCheckIn : %d %s", routeId, date);
	}
}
