#Practical project, 2013–14

## Object-Oriented Software Engineering

***

## Description

The centralized reservation system for an airline company contains the following
components:

+ A **reservation server** managing data associated to:
    - the ﬂights oﬀered by the company,
    - the tickets that have been emitted, and
    - seat number assignments for the passengers that have been checked in.

+ A client application installed in **travel agencies**, used for
    - querying the server for the availability and price of ﬂights, and
    - issuing and canceling ﬂight reservations.

+ Another client application deployed at **check-in counters** in airports, used for
    - checking in passengers for a ﬂight and assigning them seat numbers, and
    - signaling the beginning and the end of check-in for a given ﬂight.

***

## API Documentation

[See wiki](https://bitbucket.org/PiWhy/flights-rest-api/wiki/Home)

***

## How to run

To launch the server :

    $ ant server
    
To launch the client example :

    $ ant client

***

## Useful links

- [Presentation slideshow](https://docs.google.com/a/rigaux.be/presentation/d/1Vjgbkju1NUorFClNjo6MVt-l5YxdqWeu4MGAYZQ3M7E/edit?usp=sharing)
- [Qu'est-ce que REST ?](http://www.croes.org/gerald/blog/qu-est-ce-que-rest/447/)
- [Assignment page](http://www.montefiore.ulg.ac.be/~blaugraud/pages/oose/oose.php)
- [Theoretical course page](http://www.montefiore.ulg.ac.be/~boigelot/cours/se/index.html)

**Java related**

- [Jersey](https://jersey.java.net/)
- [Jackson](http://jackson.codehaus.org/)
- [JSON example with Jersey + Jackson](http://www.google.com/url?q=http%3A%2F%2Fwww.mkyong.com%2Fwebservices%2Fjax-rs%2Fjson-example-with-jersey-jackson%2F&sa=D&sntz=1&usg=AFQjCNGNxyK1w1X4llNtkPYeobj7W8Ih9Q)
- [RESTful Java client with Jersey client](http://www.google.com/url?q=http%3A%2F%2Fwww.mkyong.com%2Fwebservices%2Fjax-rs%2Frestful-java-client-with-jersey-client%2F&sa=D&sntz=1&usg=AFQjCNGzFm3XL0RrbVpPMWE84NBsRBKISw)
